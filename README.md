# Install Docker

1. Download by [LINK](https://download.docker.com/win/stable/Docker%20for%20Windows%20Installer.exe) (windows)
2. How to run [LINK](https://docs.docker.com/docker-for-windows/install/#install-docker-for-windows)

# Download linux distr -  ubuntu
##### Note. I chose ubuntu 16.04 as a last LTS version. Do not change if you do not understand

Run commands in docker
1. docker pull ubuntu:16.04

# Run your ubuntu

Run commands in docker
1. docker run -ti --rm ubuntu:16.04 bash

# Help 
Very helpful [LINK](https://docs.docker.com/engine/reference/commandline/docker/)